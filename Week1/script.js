//Declaration
function greet(name1,batch){

    return "Good Morning"+ name1+ " "+batch;
}

//function calling
let Name = "Amarnath";
let batchnum = "52";
let greeting= greet(Name,batchnum);
greet();
console.log(greeting);

//addition of 3 numbers

function additionofThreenums(num1,num2,num3){
    return num1+num2+num3;
}

let result1 = additionofThreenums(12,25,29);
let result2 = additionofThreenums(30,50,237);
console.log(result1,result2);

//EvenorOdd

function EvenorOdd(num){
    if (num%2 == 0){
        return "Even Number"
    }
    return "Odd Number"
}
console.log(EvenorOdd(8));

//write a function to print the sum of numbers

//5 1+2+3+4+5
//3 1*2*3*4*5

function sumofnumbers(n){
    let sum = 0;
    for(let i = 1 ; i<=n; i++){
        sum+= i
    }
    return sum;
}
console.log(sumofnumbers(6))

function mulofnumbers(n){
    let mul = 1;
    for(let i = 1 ; i<=n; i++){
        mul*= i
    }
    return mul;
}
console.log(mulofnumbers(6))
// if else if
//if (condition){
//   ststement
//    }else (condition){
//      Statement
//        }else   
//}
// write a function to check whether a given number is negative or anything 
function chechnumber(num){
    if(num<0){
        return "Negative number";
    }
    else if (num>0 & num<50){
        return "Between 1 and 50";
    }
    else {
    return "Above 50";
    }
}

// let number = parseInt(prompt("Enter the number"));
// console.log(typeof(number)); 
console.log(chechnumber(45))

function Table(number){
    for (let i=0;i<=10;i++){
        console.log(number+"*"+i+"="+number*i);
    }
}
Table(5);

const numbers = [12,2,5,23,30]

for(let i = 0; i<numbers.length; i++){
    console.log(numbers[i]);
}

//find the maximum value in the array
function findMaximum() {
    let Arr = [20,5,7,24,9];
 
    let maxValue = Math.max(...Arr);
     
    console.log("Maximum Element is: " + maxValue);
}
 
findMaximum()

//find the minimum value in the array
function findMinimum() {
    let Arr = [20,5,7,24,9];
 
    let minValue = Math.min(...Arr);
         
    console.log("Minimum element is: " + minValue);
    
}
 
findMinimum()

//find the sum of values in the given array

let array=[5,12,1,9,8];
let sum=0;
for(let i=0; i<array.length; i++){
    sum+= array[i];
}
console.log("sum is " + sum)


// while (condition){
//     //statement to be executed;
// }

// sum of n numbers

function sumofN(limit){
let sum =0;
let num = 1;
while(num<=limit){
    sum= sum+num;
    num++
}
    return sum
}
let result =sumofN(10);
console.log("The sum of given number is: "+ result);
console.log(`the sum of n numbers is ${result}`);

//write a program to reverse of a number by using whileloop in javascript.

function reversenum(num){
    let reversenum=0;
    while(num!=0){
        reversenum = (reversenum*10)+num%10;
        num=Math.floor(num/10);
    }
    return reversenum
}
console.log("Reverse of a given number: " + reversenum(58369));

// count the number of digits in the given number

function countofdigits(num){
    let count=0;
    while(num!=0){
        count++;
       num = Math.floor(num/10);
    }
    return count
}
console.log(countofdigits(58369))

// find the sum of digits in a given number 12345

function sumofdigits(num){
    let sum=0;
    while(num!=0){
        sum= sum+num%10;
        num= Math.floor(num/10);
    }
    return sum
}
console.log(sumofdigits(58369))

//chech whether the given number is palindrome
// reverse of a string (word)

function reverseOfString(str){
    return str.split('').reverse().join('')
}console.log(reverseOfString("Hello World"))

// check wheather the given string is palindrome

function checkPalindrome(string) {
    let arrayValues = string.split('');
    let reverseArrayValues = arrayValues.reverse();
    let reverseString = reverseArrayValues.join('');

    if(string == reverseString) {
        console.log('It is a palindrome');
    }
    else {
        console.log('It is not a palindrome');
    }
}
let string = ("1221");

checkPalindrome(string);

// find the missing number in array

function miss(num) {
    for (let i = 1; i<= num.length + 1; i++) {
      if (num.indexOf(i) === -1) 
        return i;
    }
  }
  
  num = [1,2,5,6,7,3]
  console.log("Missing number of the array: "+miss(num));

  //prime number

  function prime(n){
    if(n<2){
        return 'is not prime'
    }

  for(i=2;i<n;i++){
    if(n%i==0){
    return "is not a prime"
   }
 }
  return 'is a prime'
}
console.log(prime(2))

//factors of a number

function factor(n){
    const factors =[];
    let count=0
    for(i=1;i<=n;i++){
        if(n%i==0){
            factors.push(i)
            count++;
        }
    }
    return `factors are ${factors} and count is ${count}`;
}
console.log(factor(24));

//sum of factors of the given number

function sumoffactors(n){
    let sum=0;
    for(let i=1;i<=n;i++){
        if ((n%i)<1){
            sum+=i;
        }
    }
    return sum
}
console.log(sumoffactors(10))
//print the list of perfect number

//check whether given number is perfect or not

function perfectnum(n)
{
let sum = 0;
   for(let i=1;i<=n/2;i++)
     {
         if(n%i === 0)
          {
            sum += i;
          }
     }
   
     if(sum == n && sum !== 0)
        {
       console.log("It is a perfect number.");
        } 
     else
        {
       console.log("It is not a perfect number.");
        }   
 } 
perfectnum(6);

//push and pop

let skills =["java", "python","html","css"]

console.log(skills.length);

skills.push("Bootstrap"); // push adds the given element in the given array at last
console.log(skills);

skills.pop();  // pop removes the last element in the array
skills.pop();
console.log(skills);

//replace
let text ="Microsoft teams meeting";
let newtext = text.replace(/microsoft/i, "Google");
console.log(newtext)

// take a sentence as input , and write a function which count the words in the given sentence

function counteords(sentence){
    let words= sentence.split(' ');
    return `The number of words are ${words.length}`   
}

let sentence= "Hello i am learning javascript";
console.log(counteords(sentence))

// sum of negative numbers in array
function sumofnegNumbers(num1){
    let number1=[10,-12,20,-15,-40,26]
    let possum=0;
    let negsum=0;
    for(let i=0;i<n.length;i++){
        if(n[i]<0)
        negsum+=number1;
        
    }
    return sum;
}
console.log(sumofnegNumbers(sum))