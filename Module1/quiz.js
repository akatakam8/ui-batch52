const Questions = [{
	q: "What is capital of India?",
	a: [{ text: "Gandhinagar", isCorrect: false },
	{ text: "Surat", isCorrect: false },
	{ text: "Delhi", isCorrect: true },
	{ text: "Mumbai", isCorrect: false }
	]

},
{
	q: "What is the capital of Telangana?",
	a: [{ text: "Secbd", isCorrect: false, isSelected: false },
	{ text: "Mulugu", isCorrect: false },
	{ text: "Adilabad", isCorrect: false },
	{ text: "Hyderabad", isCorrect: true }
	]

},
{
	q: "What is the capital of AndhraPradesh",
	a: [{ text: "Vizag", isCorrect: false },
	{ text: "Vijayawada", isCorrect: false },
	{ text: "Amaravathi", isCorrect: true },
	{ text: "Sriharikota", isCorrect: false }
	]

},
{
    q: "What is the capital of Himachal Pradesh",
    a: [{ text: "Mandi", isCorrect: false},
    { text: "Shmila", isCorrect: true},
    { text: "Chamba", isCorrect: false},
    { text: "Kangra", isCorrect: false},
]
}

]

let currQuestion = 0
let score = 0

function loadQues() {
	const question = document.getElementById("ques")
	const opt = document.getElementById("opt")

	question.textContent = Questions[currQuestion].q;
	opt.innerHTML = ""

	for (let i = 0; i < Questions[currQuestion].a.length; i++) {
		const choicesdiv = document.createElement("div");
		const choice = document.createElement("input");
		const choiceLabel = document.createElement("label");

		choice.type = "radio";
		choice.name = "answer";
		choice.value = i;

		choiceLabel.textContent = Questions[currQuestion].a[i].text;

		choicesdiv.appendChild(choice);
		choicesdiv.appendChild(choiceLabel);
		opt.appendChild(choicesdiv);
	}
}

loadQues();

function loadScore() {
	const totalScore = document.getElementById("score")
	totalScore.textContent = `You scored ${score} out of ${Questions.length}`
}


function nextQuestion() {
	if (currQuestion < Questions.length - 1) {
		currQuestion++;
		loadQues();
	} else {
		document.getElementById("opt").remove()
		document.getElementById("ques").remove()
		document.getElementById("btn").remove()
		loadScore();
	}
}

function checkAns() {
	const selectedAns = parseInt(document.querySelector('input[name="answer"]:checked').value);
	if (Questions[currQuestion].a[selectedAns].isCorrect) {
		score++;
		console.log("Correct")
		nextQuestion();
	} else {
		nextQuestion();
	}
}
function formvalidation() {
    let Username = document.getElementById("username");
    let Password = document.getElementById("password");
    if (Username.Value.trim() === null || Username.value.trim() === ""){
        alert("please enter the username");
        return false;
    }
    else if (Password.value.trim() === null || Password.value.trim() === ""){
        alert("please enter the password");
        return false;
    }
    else{
        return true;
    }
}